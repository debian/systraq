<?xml version='1.0' encoding='UTF-8'?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
"http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [

  <!--

See http://www.docbook.org/tdg/en/html/ for dbx reference.

    See http://people.debian.org/~branden/talks/wtfm/ for hints
    on writing manpages.

    Headings for Manpages in Sections 1, 6, or 8

    NAME
    SYNOPSIS
    DESCRIPTION
    OPTIONS
    EXAMPLES
    EXIT STATUS
    INPUT FILES (if used)
    OUTPUT FILES (if used)
    DIAGNOSTICS (if any)
    ASYNCHRONOUS EVENTS (if handlers deﬁned)
    CONSEQUENCES OF ERRORS (if unusual)
    AUTHOR or AUTHORS
    SEE ALSO

 -->


  <!ENTITY firstname "<firstname>Joost</firstname>">
  <!ENTITY surname   "<surname>van Baal-Ilić</surname>">
  <!ENTITY section   "<manvolnum>8</manvolnum>">
  <!ENTITY username  "Joost van Baal-Ilić">
  <!ENTITY uccommand "<refentrytitle>SYSTRAQ</refentrytitle>">
  <!ENTITY command   "systraq">

  <!ENTITY dhtitle     "systraq User Manual">
  <!ENTITY dhucpackage "SYSTRAQ">
  <!ENTITY dhpackage   "systraq">

  <!ENTITY gnu         "<acronym>GNU</acronym>">
  <!ENTITY gpl         "&gnu; <acronym>GPL</acronym>">
  <!ENTITY % man.ent SYSTEM "man.ent">
%man.ent;
]>

<refentry>
  <refentryinfo>
    <title>&dhtitle;</title>
    <productname>&dhpackage;</productname>
    <address>
      &bugemail;
    </address>
    <authorgroup>
    <author>
      &firstname;
      &surname;
      <email>&bugemail;</email>
    </author>
    <author>
<firstname>Laurent</firstname> <surname>Fousse</surname>
<email>laurent@komite.net</email>
    </author>
    </authorgroup>
    <copyright>
      <year>2001-&year;</year>
      <holder>&username;</holder>
    </copyright>
    <legalnotice>
    <para>
Permission is granted to copy, distribute and/or modify this document under the
terms of the &gnu; General Public License, Version 2 or any later version
published by the Free Software Foundation.
    </para>
    <para>
This document is based upon a manual page written by Laurent Fousse for the
Debian project.
    </para>
    </legalnotice>
    &date;
  </refentryinfo>
  <refmeta>
    &uccommand;

    &section;
  </refmeta>
  <refnamediv>
    <refname>&command;</refname>

    <refpurpose>monitors your system and warns you when files change</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <cmdsynopsis>
      <command>&command;</command>
    </cmdsynopsis>
  </refsynopsisdiv>
  <refsect1>
    <title>DESCRIPTION</title>

    <para><command>&command;</command> is part of the systraq package, a set of scripts
that monitors your system for file changes. It is meant to be run by a
cronjob, not directly.

    </para>
    <para>

<command>&command;</command> runs various system commands, to inspect the state
of the system: what is it doing now?, what has it been doing recently?, are we
running to hardware limitations?.  Furthermore, it runs commands to inspect
some files in users' homedirectories, as well as some system files, for
frequently seen flaws.  All these commands are maintained in little scripts in
<filename class='directory'>&etcpathraw;/systraq.d</filename>.  The first two
characters of the script's name are used for the execution-order.  The names of
executable files in systraq.d/ (or symlinks to such files) must consist
entirely of upper and lower case letters, digits, underscores, and hyphens.
Files which not adhere will be silently ignored.  The systraq script supplies
some environment variables to the scripts in <filename
class='directory'>systraq.d/</filename>, these might be helpful when adding
your own scripts.  Refer to the (very small) <command>&command;</command> code
itself for the details.

</para>
<para>

We'll elaborate on some of the shipped <filename
class='directory'>systraq.d/</filename> scripts.

</para>
<para>

<command><replaceable>AA</replaceable>-shellrc</command> checks for unsafe
umask setting in shell startup scripts, or unsafe PATH in these scripts.

</para>
<para>

<command><replaceable>AA</replaceable>-debsums</command> runs
<command>debsums</command>, to check md5sums as stated in packaging files with
the sums of the actual files running the system.  (NB: debsums has support for
md5 checksums only, most Debian packages ship md5 checksums only.  Therefore,
we can't use sha256sum.  See also the discussion on <ulink
url='http://lists.debian.org/debian-release/2007/08/threads.html#00086'>
<citetitle>proposed release goal: DEBIAN/md5sums for all
packages</citetitle></ulink> at the Debian release mailinglist in August 2007
as well as <ulink url='http://bugs.debian.org/268658'><citetitle>Debian Bug
#268658</citetitle></ulink> for some considerations on this.)

</para>
<para>

<command><replaceable>AA</replaceable>-localdigest</command> runs
<command>sha256sum</command> (or the command set in the ST_SUM environment
variable) to check message digests as locally maintained
in a file named in the ST_LDIGESTS environment variable.  Typically, this
is set to <filename>&statepathraw;/systraq.sums</filename>.  If this
environment variable is unset, this check is silenty skipped.

</para>
<para>

Of course, you can add your own scripts.  If you name them
<command><replaceable>AA</replaceable>-local</command>, they'll never get
overwritten by any version of this software.  If you don't like one of these
scripts, you can disable it by removing the symlink, and creating a new symlink
with the same name pointing to <command>/bin/true</command>.

</para>

  </refsect1>

  <refsect1>
    <title>SEE ALSO</title>

    <para>The systraq Manual, installed in &docpath;.</para>

  </refsect1>
  <refsect1>
    <title>VERSION</title>

<para>&version;</para>

  </refsect1>
</refentry>

