<!--
  entities for docbook documents, expanded by ./setversion
-->

<!ENTITY version "@VERSION@">

<!-- <!ENTITY year SYSTEM "../stamp.year"> -->
<!ENTITY year "@YEAR@">
<!ENTITY month "@MONTH@">
<!ENTITY day "@DAY@">
<!-- <!ENTITY date "<date>&day; &month; &year;</date>"> -->
<!ENTITY date "<date>@DATE@</date>">

<!ENTITY bugemail "@PACKAGE_BUGREPORT@">

<!ENTITY etcpath "<filename class='directory'>/etc/systraq</filename>">
<!ENTITY etcpathraw "/etc/systraq">
<!ENTITY statepath "<filename class='directory'>/var/lib/systraq</filename>">
<!ENTITY statepathraw "/var/lib/systraq">
<!ENTITY binpath "<filename class='directory'>/usr/bin</filename>">
<!ENTITY sbinpath "<filename class='directory'>/usr/sbin</filename>">
<!ENTITY libexecpath "<filename class='directory'>/usr/share/systraq</filename>">
<!ENTITY docpath "<filename class='directory'>/usr/share/doc/systraq</filename>">
<!ENTITY docpathraw "/usr/share/doc/systraq">
<!ENTITY homepath "<filename class='directory'>/var/lib/systraq</filename>">
<!ENTITY homepathraw "/var/lib/systraq"> <!-- /usr/local/var/lib/systraq -->
<!ENTITY cronpathraw "/etc/cron.d"> <!-- /usr/local/etc/cron.d/ -->
<!ENTITY examplepath "<filename class='directory'>&docpathraw;/examples</filename>">
<!ENTITY examplepathraw "&docpathraw;/examples">

<!ENTITY brokenfile "<filename>&etcpathraw;/i_want_a_broken_systraq</filename>">
<!ENTITY unconfiguredfile "<filename>&etcpathraw;/systraq_is_unconfigured</filename>">
