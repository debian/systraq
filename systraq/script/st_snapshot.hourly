#!/bin/sh -e

# Copyright (C) 2005, 2008 Joost van Baal
#
# This file is part of systraq.  Systraq is free software; you can redistribute
# it and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the License,
# or (at your option) any later version.  You should have received a copy of
# the GNU General Public License along with this file (see COPYING).

# st_snapshot.hourly - wrapper for st_snapshot, to be run from cron

test -f /etc/systraq/i_want_a_broken_systraq && exit 0

if test -f /etc/systraq/systraq_is_unconfigured
then
    cat /etc/systraq/systraq_is_unconfigured
    exit 0
fi

if test -f /etc/systraq/systraq_is_not_upgraded
then
    cat /etc/systraq/systraq_is_not_upgraded
    exit 0
fi

if test -n "$ST_SUM"
then
    # user has set ST_SUM, honor that one
    :
elif command -v sha256sum >/dev/null
then
    ST_SUM=sha256sum
elif command -v md5sum >/dev/null
then
    cat <<EOT
Warning: using md5sum for computing file checksums, as fallback for preferred
sha256sum.  Please install sha256sum, it is shipped with GNU coreutils since
release 6.0 (2006-08-15).  Alternatively, set the environment variable ST_SUM
to your preferred checksum tool.

"md5sum should not be considered truly secure against malicious tampering:
although finding a file with a given MD5 fingerprint, or modifying a file so as
to retain its MD5 are considered infeasible at the moment, it is known how to
produce different files with identical MD5, something which can be a security
issue in certain contexts."  --Ulrich Drepper e.a., 2008, coreutils 6.10
EOT
    ST_SUM=md5sum
else
    cat <<EOT
Panic: sha256sum not found, environment variable ST_SUM unset.  Please install
sha256sum, it is shipped with GNU coreutils since release 6.0 (2006-08-15).
Alternatively, set the environment variable ST_SUM to your preferred checksum
tool.
EOT
    exit 1
fi

export ST_SUM

if test root = "$ST_MODE"
then
    # run in root-mode
    for f in /etc/systraq/snapshot_root.list /etc/systraq/snapshot_root.homelist
    do
        if test ! -f $f
        then
            echo File $f does not exist. Please create it.
            echo See the Systraq Manual in /usr/share/doc/systraq for instructions.
            exit 1
        fi
    done

    st_snapshot /etc/systraq/snapshot_root.list \
      /etc/systraq/snapshot_root.homelist > \
      /var/lib/systraq/snapshot_root.stat
else
    for f in /etc/systraq/snapshot_pub.list /etc/systraq/snapshot_pub.homelist
    do
        if test ! -f $f
        then
            echo File $f does not exist. Please create it.
            echo See the Systraq Manual in /usr/share/doc/systraq for instructions.
            exit 1
        fi
    done
    ST_OPHOMES=yes st_snapshot /etc/systraq/snapshot_pub.list \
      /etc/systraq/snapshot_pub.homelist > /var/lib/systraq/snapshot_pub.stat
fi
