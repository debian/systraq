<!DOCTYPE style-sheet PUBLIC "-//James Clark//DTD DSSSL Style Sheet//EN" [
<!ENTITY docbook.dsl PUBLIC "-//Norman Walsh//DOCUMENT DocBook Print Stylesheet//EN" CDATA DSSSL>
]>

<!-- This files history probably started in
     cvs/sf.net/logreport/docs/presentations/linuxkongress-2001/paper/print.dsl,
     2001-12. -->
<!-- see also print.dsl as shipped with caspar ( http://mdcc.cx/caspar/ )
     since caspar version 20020313 -->

<!--

# Copyright (C) 2001 Stichting LogReport Foundation
# Copyright (C) 2002, 2003, 2004 Joost van Baal
#
#     This program is free software; you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation; either version 2 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program (see COPYING); if not, check with
#     http://www.gnu.org/copyleft/gpl.html or write to the Free Software 
#     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.

-->

<style-sheet>
  <style-specification use="docbook">
    <style-specification-body>

;; See dbparam.dsl for definitions of refentries
(define %section-autolabel% #t)           ;; Sections are enumerated
(define %chapter-autolabel% #t)           ;; Chapters are enumerated

(define %generate-article-toc% #t)        ;; A Table of Contents should
                                          ;;  be produced for Articles

(define %generate-article-titlepage-on-separate-page% #f)
(define %generate-article-titlepage% #t)
(define %generate-article-toc-on-titlepage% #t)

;; we want it to look like default LaTeX
(define %title-font-family% "Computer Modern")
                                          ;; The font family used in titles
(define %body-font-family% "Computer Modern")
                                          ;; The font family used in body text

(define %default-quadding% 'justify)      ;; The default quadding ('start',
                                          ;; 'center', 'justify', or 'end').

(define %body-start-indent% 0em)          ;; The default indent of body text.
                                          ;; Some elements may have more or
                                          ;; less indentation.  4pi is default
                                          ;; value 

;; we have vertical whitespace between paragraphs
;; (define %para-indent%     2em)         ;; First line start-indent for
                                          ;; paragraphs (other than the first)

(define article-titlepage-recto-style
  (style
     font-family-name: %title-font-family%
     font-size: (HSIZE 1)))               ;; overrule font of title on
                                          ;; titlepage, see dbttlpg.dsl. tnx
                                          ;; flacoste

(define (article-titlepage-recto-elements)
  (list (normalize "title") 
        (normalize "subtitle") 
        (normalize "corpauthor") 
        (normalize "authorgroup") 
        (normalize "author") 
        (normalize "abstract")
        (normalize "legalnotice")))        ;; legalnotice added

;; article-titlepage-recto-elements in dbttlpg.dsl

(define (toc-depth nd) 2)                 ;; see dbautoc.dsl, default 1 for
                                          ;; article, 7 for book

(define (first-page-inner-footer gi)
  (cond
   ((equal? (normalize gi) (normalize "dedication")) (empty-sosofo))
   ((equal? (normalize gi) (normalize "lot")) (empty-sosofo))
   ((equal? (normalize gi) (normalize "part")) (empty-sosofo))
   ((equal? (normalize gi) (normalize "toc")) (empty-sosofo))
   (else
    (with-mode footer-copyright-mode
      (process-node-list (select-elements (children (current-node))
					  (normalize "articleinfo")))))))

(define (page-inner-footer gi)
  (cond
   ((equal? (normalize gi) (normalize "dedication")) (empty-sosofo))
   ((equal? (normalize gi) (normalize "lot")) (empty-sosofo))
   ((equal? (normalize gi) (normalize "part")) (empty-sosofo))
   ((equal? (normalize gi) (normalize "toc")) (empty-sosofo))
   (else
    (with-mode footer-id-mode
      (process-node-list (select-elements (children (current-node))
					  (normalize "articleinfo")))))))

;; get a small font used for legalnotice para on titlepage
;; override defaults in 
;; /usr/share/sgml/docbook/stylesheet/dsssl/modular/print/dbttlpg.dsl
(mode article-titlepage-recto-mode
  (element (legalnotice para)
    (make paragraph
      use: article-titlepage-recto-style
      ;; quadding: 'start
      line-spacing: (* 0.6 (inherited-line-spacing))     ;; default: 0.8
      font-size: (* 0.6 (inherited-font-size))           ;; default: 0.8
      (process-children))))




(mode footer-id-mode
  ;; Prevent elements with PCDATA content 
  ;; from being processed
  (element title (empty-sosofo))
  (element subtitle (empty-sosofo))
  (element copyright (empty-sosofo))
  (element author (empty-sosofo))
  (element revnumber (empty-sosofo))
  (element date (empty-sosofo))

  ;; no legalnotice here, JvB
  (element legalnotice (empty-sosofo))

  (element revremark (process-children-trim))

  (default (process-children-trim)))


(define (make-footer-rule)
  (make rule
    orientation: 'escapement
    position-point-shift: 0.75cm
    length: 5cm
    layer: 1
    line-thickness: 0.4pt))

(mode footer-copyright-mode
  ;; Prevent elements with PCDATA content 
  ;; from being processed
  (element title (empty-sosofo))
  (element subtitle (empty-sosofo))
  (element author (empty-sosofo))
  (element revnumber (empty-sosofo))
  (element date (empty-sosofo))
  (element revhistory (empty-sosofo))

  ;; no legalnotice here, JvB
  (element legalnotice (empty-sosofo))

  (element copyright 
    (let ((year (select-elements (children (current-node))
				 (normalize "year")))
	  (holder (select-elements (children (current-node))
				 (normalize "holder"))))
      (make sequence
	(make-footer-rule)
	(literal "Copyright &#169; ")
	(process-node-list year)
	(literal " ")
	(process-node-list holder))))

  (default
    (process-children-trim)))

    </style-specification-body>
  </style-specification>
  <external-specification id="docbook" document="docbook.dsl">
</style-sheet>

