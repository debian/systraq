<!DOCTYPE style-sheet PUBLIC "-//James Clark//DTD DSSSL Style Sheet//EN" [
<!ENTITY docbook.dsl PUBLIC "-//Norman Walsh//DOCUMENT DocBook HTML Stylesheet//EN" CDATA DSSSL>
]>

<!-- This files history probably started in
     cvs/sf.net/logreport/docs/presentations/linuxkongress-2001/paper/html.dsl,
     2001-12. -->
<!-- see also html.dsl as shipped with caspar ( http://mdcc.cx/caspar/ )
     since caspar version 20020313 -->

<!--

# Copyright (C) 2001 Stichting LogReport Foundation
# Copyright (C) 2002, 2003, 2004 Joost van Baal
#
#     This program is free software; you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation; either version 2 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program (see COPYING); if not, check with
#     http://www.gnu.org/copyleft/gpl.html or write to the Free Software·
#     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.

-->

<style-sheet>
<style-specification use="docbook">
<style-specification-body>

;; See dbparam.dsl for definitions of refentries
(define %section-autolabel% #t)           ;; Sections are enumerated
(define %chapter-autolabel% #t)           ;; Chapters are enumerated

(define %generate-article-toc% #t)        ;; A Table of Contents should
                                          ;;  be produced for Articles

(define %generate-article-titlepage-on-separate-page% #f)
(define %generate-article-titlepage% #t)
(define %generate-article-toc-on-titlepage% #t)

(define nochunks #t)                    ;; Dont make multiple pages
(define rootchunk #t)                   ;; Do make a 'root' page
(define %use-id-as-filename% #t)        ;; Use book id as filename
(define %html-ext% ".html")             ;; give it a proper html extension

</style-specification-body>
</style-specification>
<external-specification id="docbook" document="docbook.dsl">
</style-sheet>
