# /etc/cron.d/systraq - cronjob for systraq jobs
#
# This PATH is useful if you've used a non-standard bindir during ./configure
PATH=/usr/sbin:/usr/bin:/sbin:/bin
#
# m h dom mon dow usercommand
#
# run filetraq every hour at :30.
30 * * * *   debian-systraq  [ -r /etc/systraq/filetraq.conf ] && command -v filetraq >/dev/null && filetraq /etc/systraq/filetraq.conf /var/lib/systraq/filetraq
#
# clean up filetraq backup files older than two months
@monthly     debian-systraq  [ -d /var/lib/systraq/filetraq ] && find /var/lib/systraq/filetraq -type f -name "*.??.??__??.??" -ctime +60 -exec rm \{\} \;
#
# sent the weekly systraq email
@weekly       debian-systraq  command -v systraq >/dev/null && systraq | mailx -s "[Systraq] `hostname` `date +\%Y\%m\%d`" debian-systraq
#
# update the systraq status files
0 * * * *    debian-systraq  command -v st_snapshot.hourly >/dev/null && st_snapshot.hourly
0 * * * *    root            command -v st_snapshot.hourly >/dev/null && ST_MODE=root st_snapshot.hourly
#
