# /usr/include/systraq/filetraq.mk - included by /etc/systraq/Makefile

filetraq.conf: filetraq.main.conf filetraq.tail.conf
	echo '# $@: generated from $^' | \
          cat - filetraq.main.conf filetraq.tail.conf > $@

filetraq.main.conf:
	echo '# $@: automatically generated' > $@
	find /etc -not -readable -and -prune -or \( -perm -a+r -and -type f -and -print \) | sort >> $@
	ls -1 /home/*/.ssh/a* | sort >> $@

.PHONY: filetraq.main.conf
