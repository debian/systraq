Source: systraq
Section: admin
Priority: optional
Maintainer: Laurent Fousse <laurent@komite.net>
Uploaders: Joost van Baal <joostvb@debian.org>
Build-Depends: debhelper (>= 4.1.0), cdbs, quilt
Build-Depends-Indep: jade, sgml-data, w3m, jadetex, docbook-dsssl, docbook-xml, docbook-xsl, openjade, xsltproc
Standards-Version: 3.7.3
Homepage: http://mdcc.cx/systraq

Package: systraq
Architecture: all
Depends: ${shlibs:Depends}, ${misc:Depends}, exim4-daemon-light | mail-transport-agent, adduser, filetraq (>= 0.2-10), procps, net-tools, debsums
Description: monitor your system and warn when system files change
 Systraq daily sends you an email listing the state of your system.
 Furthermore, if critical files have changed, you'll get an email within a
 shorter notice. Systraq consists of few very small shell scripts.
 .
 It can help you implementing a not too strict security policy.  For more
 demanding systems, you'd better use something like tripwire. Make sure you
 really want to do the monitoring this script offers: it might not comply with
 your site's privacy policy. Getting informed when users' config file change
 might be too intrusive. 
 .
 Other similar tools are available in Debian (`diffmon' for instance), but
 systraq is less intrusive because it can warn for file changes without mailing
 a complete diff (which is not desirable for e.g. /etc/shadow).
