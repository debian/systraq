systraq (0.0.20070301-1) unstable; urgency=low

  * New upstream release.  Fixes bug: "systraq cannot handle filenames with
    spaces" (closes: #409777).
  * debian/README.Debian: add note on how to disable or add scripts to
    systraq.d.
  * debian/systraq.8: removed; now shipped with upstream.

 -- Joost van Baal <joostvb@debian.org>  Sun, 04 Mar 2007 12:24:20 +0100

systraq (0.0.20070118-2) unstable; urgency=low

  * debian/control: add homepage url.
  * debian/{rules,conffiles,postinst}: no longer have symlinks in
    /etc/systraq/systraq.d be part of the package (as
    conffiles), but create/maintain these in postinst.  We no longer rely
    on dpkg for managing these.  This caused systraq's daily reports to
    be empty, and the symlinks in /etc/systraq/systraq.d/ to be broken.
    (Closes: #408359).
  * debian/postinst: run under "set -e". only do real stuff when called with
    argument "configure".
  * debian/postinst: tweaked message from cron in
    /etc/systraq/systraq_is_unconfigured.

 -- Joost van Baal <joostvb@debian.org>  Fri, 02 Feb 2007 11:51:35 +0100

systraq (0.0.20070118-1) unstable; urgency=low

  * New upstream release.  Fixes bug: "systraq cron job is noisy after systraq
    is removed but not purged" (closes: #406149).
  * debian/dirs: removed: upstream Makefile creates all needed dirs.
  * debian/patches/{series,001_debian_systraq_user.diff},
    debian/{control,rules}: manage our patch on /etc/cron.d/systraq
    using quilt, so that it can be maintained using svn.
  * debian/rules: install arch-independent scripts in /usr/share/systraq,
    not /usr/lib.
  * debian/rules: no longer installs upstream changelog as two files.
  * debian/control: Build-Depend on debhelper (>= 4.1.0): current cdbs needs
    that.
  * debian/doc-base.manual: added: register systraq manual using doc-base.

 -- Joost van Baal <joostvb@debian.org>  Thu, 18 Jan 2007 15:23:52 +0100

systraq (0.0.20050213-8) unstable; urgency=low

  * debian/postrm: do not assume the non-essential adduser package is
    installed at package purge time (closes: #398538).
  * debian/control: added myself to Uploaders, after ACK from Laurent
    (see Bug#398538).
  * debian/control: Standards updated, no changes needed.
  * debian/control: cdbs and debhelper moved from Build-Depends-Indep to
    Build-Depends (thanks lintian).
  * debian/copyright: New FSF snailmail address.

 -- Joost van Baal <joostvb@debian.org>  Sun, 19 Nov 2006 13:06:51 +0100

systraq (0.0.20050213-7) unstable; urgency=low

  * Changed packaging to use cdbs.
  * Set correct ownership of /var/lib/systraq/.forward on install
    (closes: #329282).
  * Added note about possible email delivery failures in README.Debian.

 -- Laurent Fousse <laurent@komite.net>  Tue, 27 Sep 2005 17:28:57 +0200

systraq (0.0.20050213-6) unstable; urgency=low

  * Changed one remaining "systraq" to "debian-systraq" in the cron file,
    could cause email delivery to fail.
  * Modified postinst to not warn about the old username if the new username
    is already in use.
  * Updated Standards-Version to 3.6.2, no change needed.

 -- Laurent Fousse <laurent@komite.net>  Thu, 11 Aug 2005 11:30:11 +0200

systraq (0.0.20050213-5) unstable; urgency=low

  * The new systraq username is now `debian-systraq'. Now we can do the right
    thing on purge (closes: #315683).

 -- Laurent Fousse <laurent@komite.net>  Fri, 29 Jul 2005 15:11:56 +0200

systraq (0.0.20050213-4) unstable; urgency=low

  * Tagged files in /etc/systaq/systraq.d as conffiles. Closes: #297531.

 -- Laurent Fousse <laurent@komite.net>  Tue,  1 Mar 2005 12:36:16 +0100

systraq (0.0.20050213-3) unstable; urgency=low

  * systraq_is_unconfigured should only be created if systraq *is*
    unconfigured. This is now fixed.

 -- Laurent Fousse <laurent@komite.net>  Sun, 13 Feb 2005 21:18:36 +0100

systraq (0.0.20050213-2) unstable; urgency=low

  * Install /etc/systraq/systraq_is_unconfigured as it is no longer
    installed by upstream's makefile.

 -- Laurent Fousse <laurent@komite.net>  Sun, 13 Feb 2005 20:42:14 +0100

systraq (0.0.20050213-1) unstable; urgency=low

  * New upstream release.

 -- Laurent Fousse <laurent@komite.net>  Sun, 13 Feb 2005 18:11:52 +0100

systraq (0.0.20050209-1) unstable; urgency=low

  * New upstream release.
    + better fix for hourly cron mail annoyance (closes: #289791).
  * Use local DTD for documentation validation (closes: #291619).

 -- Laurent Fousse <laurent@komite.net>  Fri, 11 Feb 2005 16:16:00 +0100

systraq (0.0.20041118-2) unstable; urgency=low

  * Install a working yet minimal configuration so that cronjobs don't
    yell at you (closes: #289791).

 -- Laurent Fousse <laurent@komite.net>  Tue, 11 Jan 2005 12:15:23 +0100

systraq (0.0.20041118-1) unstable; urgency=low

  * First actual debian upload.
  * Thanks Joost for the packaging help.

 -- Laurent Fousse <laurent@komite.net>  Wed,  8 Dec 2004 21:24:07 +0100

systraq (0.0.20041118-0.3) unstable; urgency=low

  * Non-maintainer build.
  * postinst: fix typo.

 -- Joost van Baal <joostvb@debian.org>  Thu, 18 Nov 2004 22:19:20 +0100

systraq (0.0.20041118-0.2) unstable; urgency=low

  * Non-maintainer build.
  * postinst: Do something sane when systraq homedirectory or group is
    missing from the system.  This likely happens on systems where systraq
    has been installed from the tarball.
  * rules: clean up comments.
  * This version is not to be uploaded to the Debian archive.

 -- Joost van Baal <joostvb@debian.org>  Thu, 18 Nov 2004 22:10:23 +0100

systraq (0.0.20041118-0.1) unstable; urgency=low

  * Non-maintainer build.
  * New upstream release.
  * etc/systraq.in: don't name the systraq user "debian-systraq" but just
    "systraq".  This patch to upstream is no longer needed.
  * postinst: systraq user called systraq.  Made the adduser call quiet.
    No longer edit /etc/aliases, but create ~systraq/.forward: less
    intrusive.  Don't try to create a filetraq.conf.  (No stuff is removed
    from postinst, I've just commented it out.)
  * TODO: added
  * This version is not published.

 -- Joost van Baal <joostvb@debian.org>  Thu, 18 Nov 2004 21:42:57 +0100

systraq (0.0.20041015.cvs.9-1) unstable; urgency=low

  * New upstream release

 -- Laurent Fousse <laurent@komite.net>  Mon,  1 Nov 2004 21:54:19 +0100

systraq (0.0.20041015.cvs.7-1) unstable; urgency=low

  * New upstream release.

 -- Laurent Fousse <laurent@komite.net>  Fri, 15 Oct 2004 12:15:28 +0200

systraq (0.0.20040804-1) unstable; urgency=low

  * Initial Release (closes: #276627).

 -- Laurent Fousse <laurent@komite.net>  Tue, 12 Oct 2004 17:51:59 +0200

